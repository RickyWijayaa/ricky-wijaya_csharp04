﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_7
{
    public class Person
    {
        public Person(string name, string address, EnumEthnic ethnic , DateTime dateOfBirth )
        {
            Name = name;
            Address = address;
            Ethnic = ethnic;
            DateOfBirth = dateOfBirth;
        }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age
        {
            get
            {
                return new DateTime(DateTime.Now.Subtract(DateOfBirth).Ticks).Year - 1;
            }
        }
        public EnumEthnic Ethnic { get; set; }
    }

    public enum EnumEthnic
    {
        Asian = 0,
        American = 1,
        Russian = 2,
        Malays = 3
    }

}

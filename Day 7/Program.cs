﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day_7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> persons = new List<Person>();
            persons.Add(new Person("Ricky Wijaya","Tanjungpinang",EnumEthnic.Asian,new DateTime(2001, 09, 15)));
            persons.Add(new Person("Jordan","Batam",EnumEthnic.American,new DateTime(2002, 01, 15)));
            persons.Add(new Person("Tanujaya","Bandung",EnumEthnic.Malays,new DateTime(2003, 02, 20)));
            persons.Add(new Person("Derrick","Surabaya",EnumEthnic.Russian,new DateTime(2004, 03, 30)));
            persons.Add(new Person("Tio","Jakarta",EnumEthnic.American,new DateTime(2001, 04, 28)));
            persons.Add(new Person("Andrian","Paris",EnumEthnic.Asian,new DateTime(2000, 05, 24)));
            persons.Add(new Person("Winata","Jawa",EnumEthnic.Asian,new DateTime(1999, 06, 22)));
            persons.Add(new Person("Elvin","Belanda",EnumEthnic.Malays,new DateTime(2000, 07, 8)));
            persons.Add(new Person("Valentino","China",EnumEthnic.Russian,new DateTime(1998, 08, 10)));
            persons.Add(new Person("Kevin","Surabaya",EnumEthnic.Russian,new DateTime(2001, 09, 12)));

            List<Person> FilterHasilUmur = persons.FilterUmurLebih(20);
            List<Person> HasilFilterEthnic = persons.FilterEthnic(persons[0].Ethnic);
        }
    }
}

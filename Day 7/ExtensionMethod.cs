﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day_7
{
    public static class ExtensionMethod
    {
        public static List<Person> FilterUmurLebih(this List<Person> obj, int age)
        {
            return obj.Where(x => x.Age >= age).ToList();
        }

        public static List<Person> FilterEthnic(this List<Person> obj , EnumEthnic ehtnic)
        {
            return obj.Where(x => x.Ethnic == ehtnic).ToList();
        }

    }
}
